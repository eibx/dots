if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- &> /dev/null
    end
end

function fish_prompt
	set branch (git branch 2>&1 | grep "*" | sed 's/* /:/')
    set files (git status -s 2>&1 | grep "^[^f]" | wc -l)
    if [ $files -ne 0 ]
        set files_text (set_color red)"+$files"
    end

    printf (set_color normal)(pwd | sed "s|^$HOME|~|")(set_color blue)"$branch""$files_text"
    printf (set_color normal)'> '
end

function fish_greeting
    # urxvt has a bug - this is a temp fix for it.
    tput clear
    echo (set_color normal)\# (set_color brblue)(whoami)(set_color normal)@(set_color blue)(hostname)
    echo ""
end

function fish_user_key_bindings
end

alias dots='git --git-dir=/$HOME/.dots/ --work-tree=/'
alias fd='fd -u'
# bun
set --export BUN_INSTALL "$HOME/.bun"
set --export PATH $BUN_INSTALL/bin $PATH
