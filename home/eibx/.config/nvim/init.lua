-- Basic settings
--------------------------------------------------
vim.g.mapleader = " "
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.autoindent = true
vim.opt.mouse = "a"
vim.opt.swapfile = false
vim.opt.splitright = true
vim.opt.hlsearch = false
vim.api.nvim_create_autocmd("FileType", {
  pattern = "*",
  callback = function()
    vim.opt_local.formatoptions:remove({ 'r', 'o' })
  end,
})

-- Spaces
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.shiftround = true
vim.opt.expandtab = true


-- Bindings
--------------------------------------------------
vim.api.nvim_set_keymap('n', 's', ':w<cr>', {})
vim.keymap.set('n', '<F9>', function()
    vim.cmd('vsp /home/eibx/.config/nvim/init.lua');
end)

vim.keymap.set('n', '<Leader>r', function()
    save_cursor = vim.fn.getpos(".")
    vim.cmd([[%s/\s\+$//e]])
    vim.fn.setpos(".", save_cursor)
    vim.api.nvim_command('write')
end)

vim.keymap.set('n', '<Leader>l', function()
    vim.api.nvim_feedkeys("cc//\x1b75A-\x1bA//\x1b", "n", true);
end)

-- Plugins
--------------------------------------------------
require("plugins")

-- Color theme
require("catppuccin").setup({
    flavour = "auto", -- latte, frappe, macchiato, mocha
    background = { -- :h background
        light = "latte",
        dark = "mocha",
    },
    transparent_background = false, -- disables setting the background color.
    show_end_of_buffer = false, -- shows the '~' characters after the end of buffers
    term_colors = false, -- sets terminal colors (e.g. `g:terminal_color_0`)
    dim_inactive = {
        enabled = false, -- dims the background color of inactive window
        shade = "dark",
        percentage = 0.15, -- percentage of the shade to apply to the inactive window
    },
    no_italic = true, -- Force no italic
    no_bold = false, -- Force no bold
    no_underline = false, -- Force no underline
    default_integrations = true,
    integrations = {
        cmp = true,
        gitsigns = true,
        nvimtree = true,
        treesitter = true,
        notify = false,
        mini = {
            enabled = true,
            indentscope_color = "",
        },
    },
})

vim.cmd.colorscheme "catppuccin"

-- Telescope
require("telescope").setup()

local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>p', builtin.fd, {})
vim.keymap.set('n', '<leader>f', builtin.live_grep, {})

-- Treesitter
require("nvim-treesitter.configs").setup({
  highlight = { enable = true }
})

-- Spectre
vim.keymap.set('n', '<leader>h', '<cmd>lua require("spectre").toggle()<CR>');

-- Yanky
vim.keymap.set({"n","x"}, "p", "<Plug>(YankyPutAfter)")
vim.keymap.set({"n","x"}, "P", "<Plug>(YankyPutBefore)")
vim.keymap.set({"n","x"}, "gp", "<Plug>(YankyGPutAfter)")
vim.keymap.set({"n","x"}, "gP", "<Plug>(YankyGPutBefore)")
vim.keymap.set("n", "[p", "<Plug>(YankyPreviousEntry)")
vim.keymap.set("n", "]p", "<Plug>(YankyNextEntry)")

-- mini.align
require('mini.align').setup()


-- Debug
--------------------------------------------------
function start_debug()
    local filename = vim.api.nvim_buf_get_name(0)
    local line, column = unpack(vim.api.nvim_win_get_cursor(0))
    vim.fn.jobstart("make debug file=" .. filename .. ":" .. line.. "")
end

vim.keymap.set('n', '<F5>', start_debug)

-- LSP
--------------------------------------------------

