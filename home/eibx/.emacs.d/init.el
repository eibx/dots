;; packages
(load "~/.emacs.d/packages-repository")
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)

(unless package-archive-contents
  (package-refresh-contents))
(package-install-selected-packages)

;; basic settings
(global-syntax-subword-mode)
(setq syntax-subword-skip-spaces t)
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-scroll-amount '(3))
(ivy-mode 1)
(setq ivy-on-del-error-function #'ignore)
(winner-mode 1)


;; visual settings
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-display-line-numbers-mode 1)
(setq display-line-numbers-width 3)
(setq cursor-in-non-selected-windows nil)


;; visual settings / mode line
(set-face-attribute 'mode-line nil
    :background "#222536"
    :foreground "white"
    :box '(:line-width 2 :color "#222536")
    :font "FiraCode-9")
(set-face-attribute 'mode-line-inactive nil
    :background "#181a26"
    :foreground "#363a55"
    :box '(:line-width 2 :color "#181a26"))
(set-face-attribute 'mode-line-buffer-id nil
    :background nil
    :foreground nil)
(set-face-font 'default "FiraCode-10")


;; visual settings / whitespace
(setq whitespace-style (quote (face spaces tabs trailing)))
(global-whitespace-mode 1)


;; disable auto-save and auto-backup
(setq auto-save-default nil)
(setq make-backup-files nil)
(setq create-lockfiles nil)


;; keybindings
;; example: (global-set-key (kbd "C-z") 'undo)
(defun custom-quit()
  (interactive)
  (if (get-buffer "*compilation*")
      (progn
        ; Remove window - if before build there only was 1 window
        (if (= last-compile-window-count 1)
            (delete-windows-on (get-buffer "*compilation*")))
  	    (kill-buffer "*compilation*")
        )
    )
  (call-interactively (key-binding (kbd "C-g")))
  )

;; keybindings / basics
(global-set-key (kbd "<escape>") 'custom-quit)
(global-set-key (kbd "C-<escape>") (lambda() (interactive)(kill-buffer "*compilation*")))
(global-set-key (kbd "<home>") 'beginning-of-line-text)
(global-set-key (kbd "<end>") 'end-of-line)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-o") 'find-file)
(global-set-key (kbd "C-a") 'mark-whole-buffer)
(global-set-key (kbd "C-S-P") 'execute-extended-command)
(global-set-key (kbd "C-=") 'delete-trailing-whitespace)
(global-set-key (kbd "<f9>") (lambda() (interactive)(find-file "~/.emacs.d/init.el")))
(global-set-key (kbd "S-<f9>") 'eval-buffer)
(global-set-key (kbd "S-<f5>") (lambda() (interactive)(save-buffer) (interactive)(revert-buffer t t)))
(global-set-key "\C-x2" (lambda () (interactive)(split-window-vertically) (other-window 1)))
(global-set-key "\C-x3" (lambda () (interactive)(split-window-horizontally) (other-window 1)))
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-v"))
(global-unset-key (kbd "C-t"))

;; keybindings / windows
(global-set-key (kbd "M-<left>") 'other-window)
(global-set-key (kbd "M-<right>") 'other-window)
(global-set-key (kbd "M-<up>") 'delete-other-windows)
(global-set-key (kbd "M-<down>") 'delete-window)
(global-set-key (kbd "M-k") 'kill-this-buffer)
(global-set-key (kbd "C-M-<left>") 'previous-buffer)
(global-set-key (kbd "C-M-<right>") 'next-buffer)

;; keybindings / search / swiper
(global-unset-key (kbd "C-f"))
(global-set-key (kbd "C-f") 'swiper-region)
(global-set-key (kbd "C-S-f") 'counsel-ag-region)
(global-set-key (kbd "M-<prior>") 'previous-error)
(global-set-key (kbd "M-<next>") 'next-error)
(global-set-key (kbd "C-S-g") 'goto-line)
(global-set-key (kbd "<f3>") (lambda () (interactive) (search-forward (car swiper-history))))
(define-key isearch-mode-map (kbd "C-f") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "<return>") 'isearch-repeat-forward)

(defun swiper-region ()
  "Use current region if active for swiper search"
  (interactive)
  (if (use-region-p)
      (progn
        (deactivate-mark)
        (swiper (format "%s" (buffer-substring (region-beginning) (region-end))))
        )
      (swiper)))

(defun counsel-ag-region ()
  "Use current region if active for swiper ag search"
  (interactive)
  (if (use-region-p)
      (progn
        (deactivate-mark)
        (counsel-ag (format "%s" (buffer-substring (region-beginning) (region-end))))
        )
    (counsel-ag)))


;; indentation
(setq-default c-basic-offset 4)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)


;; mark mode
(transient-mark-mode 1)
(delete-selection-mode 1)


;; projectile
(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c C-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-p") 'projectile-find-file)
(setq projectile-completion-system 'ivy)
(setq projectile-indexing-method 'hybrid)
(setq projectile-globally-ignored-file-suffixes '("blend" "mdl" "glb" "png" "qoi" "spv"))


;; undo tree
(global-undo-tree-mode)


;; multiple-cursors
(global-set-key (kbd "C-k") 'nil)
(global-set-key (kbd "C-d") 'mc/mark-next-like-this)
(global-set-key (kbd "C-k C-d") 'mc/skip-to-next-like-this)
(global-set-key (kbd "C-k C-u") 'mc/skip-to-previous-like-this)
(global-set-key (kbd "C-L") 'mc/mark-all-like-this)
(require 'cc-mode)
(define-key c++-mode-map (kbd "C-d") 'nil)
(define-key c-mode-map (kbd "C-d") 'nil)


;; golden ratio screen scroll
(require 'golden-ratio-scroll-screen)
(global-set-key (kbd "<prior>") 'golden-ratio-scroll-screen-down)
(global-set-key (kbd "<next>") 'golden-ratio-scroll-screen-up)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)


;; glsl mode
(add-to-list 'auto-mode-alist '("\\.glsl\\'" . c-mode))
(add-to-list 'auto-mode-alist '("\\.vert\\'" . c-mode))
(add-to-list 'auto-mode-alist '("\\.frag\\'" . c-mode))


;; insert
(defun insert-seperator ()
  (interactive)
  (insert "//----------------------------------------------------------------------------//"))
(global-set-key (kbd "C-l") 'insert-seperator)


;; code jumping
(defun skip-to-trailing-whitespace ()
  "Move point forward to next trailing whitespace."
  (interactive)
  (search-forward-regexp "\s$" nil t))

(global-set-key (kbd "C-]") 'skip-to-trailing-whitespace)

(defun jump-backwards-to-function()
  "Move backwards to function"
  (interactive)
  (previous-line)
  (search-backward-regexp "[a-zA-Z0-9<>*]+ [a-z_0-9]+\([^\)]*\) {")
  (end-of-line)
  (recenter))
(defun jump-forward-to-function ()
  "Move forward to function"
  (interactive)
  (next-line)
  (search-forward-regexp "[a-zA-Z0-9<>*]+ [a-z_0-9]+\([^\)]*\) {")
  (end-of-line)
  (recenter))

(global-set-key (kbd "C-<prior>") 'jump-backwards-to-function)
(global-set-key (kbd "C-<next>") 'jump-forward-to-function)

(defun jump-section-up()
  "Move up one section"
  (interactive)
  (previous-line)
  (backward-paragraph)
  (next-line)
  (beginning-of-line-text))
(defun jump-section-down()
  "Move down one section"
  (interactive)
  (forward-paragraph)
  (next-line)
  (beginning-of-line-text))

(global-set-key (kbd "C-<up>") 'jump-section-up)
(global-set-key (kbd "C-<down>") 'jump-section-down)


;; comiple / debug
(setq compile-command "make -k build")
(setq projectile-project-compilation-cmd "make -k build")
(setq compilation-read-command nil)
(setq last-compile-window-count 0)

(defun custom-compile()
  (interactive)
  (progn
    (setq last-compile-window-count (length (window-list)))
    (save-buffer)
    (call-interactively 'projectile-compile-project)
    )
  )
(global-set-key (kbd "C-b") 'custom-compile)

(defun find-bin-executable()
  (shell-command-to-string "find -executable -type f ! -name \"*.*\" ! -name \"Makefile\" ! -path \"*.git*\" | tr -d '\n'"))

(global-set-key
 (kbd "<f5>")
 (lambda () (interactive)
   (save-buffer)
   (let ((default-directory (projectile-project-root)))
     (shell-command (format "make build && gf2 -ex 'file %s' -ex 'b %s:%s' -ex 'run'"
                            (find-bin-executable)
                            (buffer-file-name)
                            (line-number-at-pos)))
     )))
